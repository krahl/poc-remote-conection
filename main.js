const {app,BrowserWindow,ipcMain } = require("electron");

const packageInfo = require(__dirname+"/package.json");

const appName = "Octopus Connect";
const appIcon = __dirname+'/assets/images/megaphone.ico';
let mainWindow = null;

function criaMainWindow(){
  mainWindow  = new BrowserWindow({
    show: false,
    title: appName+" v"+packageInfo.version,
    icon: appIcon,
    minWidth: 1100,
    minHeight: 500,
    width: 1250,
    heigth: 500
  });
  mainWindow.setMenuBarVisibility(false);
  mainWindow.loadURL(`file://${__dirname}/views/data.html`);
}

function openapp(){
  criaMainWindow();
    mainWindow.once('ready-to-show', () => {
      mainWindow.show();
    });
}

app.on("ready", ( ) =>{
    openapp();
});

app.on('window-all-closed', () => {
  app.quit()
});

ipcMain.on('fechar-sistema', () => {
  app.exit(0);
});