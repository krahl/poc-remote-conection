const { ipcRenderer } = require('electron');
const fs = require('fs');
const PowerShell = require("powershell");
const JsonService = require('../services/json');
const packageInfo = require("../package.json");
let dataTableBody = document.querySelector("#dataTableBody");
const defaultConnectionFile = 'screen mode id:i:2\nuse multimon:i:0\ndesktopwidth:i:1366\ndesktopheight:i:768\nsession bpp:i:32\nwinposstr:s:0,1,328,0,1128,600\ncompression:i:1\nkeyboardhook:i:2\naudiocapturemode:i:0\nvideoplaybackmode:i:1\nconnection type:i:7\nnetworkautodetect:i:1\nbandwidthautodetect:i:1\ndisplayconnectionbar:i:1\nenableworkspacereconnect:i:0\ndisable wallpaper:i:0\nallow font smoothing:i:0\nallow desktop composition:i:0\ndisable full window drag:i:1\ndisable menu anims:i:1\ndisable themes:i:0\ndisable cursor setting:i:0\nbitmapcachepersistenable:i:1\naudiomode:i:0\nredirectprinters:i:1\nredirectcomports:i:0\nredirectsmartcards:i:1\nredirectclipboard:i:1\nredirectposdevices:i:0\nautoreconnection enabled:i:1\nauthentication level:i:2\nprompt for credentials:i:0\nnegotiate security layer:i:1\nremoteapplicationmode:i:0\nalternate shell:s:\nshell working directory:s:\ngatewayhostname:s:\ngatewayusagemethod:i:4\ngatewaycredentialssource:i:4\ngatewayprofileusagemethod:i:0\npromptcredentialonce:i:0\ngatewaybrokeringtype:i:0\nuse redirection server name:i:0\nrdgiskdcproxy:i:0\nkdcproxyname:s:'
//ADD
let ip = document.querySelector("#ip");
let user = document.querySelector("#user");
let password = document.querySelector("#password");

let salvarButton = document.querySelector("#salvar");

function limpaTabela(){
    for (let i = dataTableBody.rows.length-1; i >= 0; i--) {
        dataTableBody.deleteRow(i);
    }
  }

function addRow(rowObj){
    let row = dataTableBody.insertRow(rowObj.index);
    for (let i = 0; i < rowObj.itens.length; i++) {
        row.insertCell(i).innerHTML = rowObj.itens[i];
    }
}

function load(){
    limpaTabela();

    JsonService.getDados().then( (dados) => {
        if (dados && dados[0]){
            dados.forEach( (row) => {
                addRow({
                    index: 0,
                    itens:[
                        row.ip,
                        row.user,
                        "********",
                        `<a href="${row.file}" class="conectar">Conectar</a>`         
                        ]
                });
            })
        }
        
    }).catch( (err) => {
        console.log("pau")
    });
    
  
  }

window.onload = () => {
    load();
}

salvarButton.addEventListener('click', () => {
    let ps = new PowerShell(`("${password.value}" | ConvertTo-SecureString -AsPlainText -Force) | ConvertFrom-SecureString;`);

    ps.on("output", data => {
        let conteudo = defaultConnectionFile + `\nfull address:s:${ip.value}\nusername:s:${user.value}\npassword 51:b:${data}\n`;

        fs.writeFile(`C:\\Users\\lucas\\Desktop\\${ip.value}.rdp`, conteudo, function(err) {
            if(err) {
                return console.log(err);
            }else{
                JsonService.salvaDados("insert",{ip: ip.value, user: user.value, file:`C:\\Users\\lucas\\Desktop\\${ip.value}.rdp`,  conteudo: conteudo}).then( (ok) => {
                    load();
                }).catch( (err) => {
                    load();
                });
            }
            
            console.log("The file was saved!");
        });
        //console.log(data);
    });
    
});