function multFilter(ignoraADireita) {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("inputSearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("pipelines");
  tr = table.getElementsByTagName("tr");
  var entrou = false;
  ignoraADireita += 1;
  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      for (var j = tr[i].getElementsByTagName("td").length - ignoraADireita; j >= 0; j--) {
        td = tr[i].getElementsByTagName("td")[j];
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
          entrou = true;
          tr[i].style.display = "";
        } else {
          if(!entrou){
            tr[i].style.display = "none";
          }
        }
      }
      entrou = false;
    } 
  }
};

function getCache(chaveCache) {
  var filter = sessionStorage.getItem(chaveCache);
  var elemento = document.getElementById('inputSearch');
  elemento.value = filter;
  elemento.onkeyup();
};

function multFilterCache(ignoraADireita, chaveCache) {
  sessionStorage.setItem(chaveCache, document.getElementById("inputSearch").value);
  multFilter(ignoraADireita);
};

function escondeSearch(){
  var elemento = document.getElementById('divSearch');
  if(elemento.getAttribute("hidden")){
    elemento.removeAttribute('hidden');    
  }else{
    elemento.setAttribute('hidden','hidden'); 
  }
  document.getElementById("inputSearch").value = "";
  multFilter(2)
};