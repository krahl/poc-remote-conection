const jsonFile = require("jsonfile");
const fs = require("fs");
const arquivo = "C:\\Users\\lucas\\Desktop\\poc.json";

let self = module.exports = {
  salvaJson: (dados) =>{
    return new Promise((success, reject) => {
    //  if ( !fs.existsSync(arquivo) ) return reject(false);
      jsonFile.writeFile(arquivo, dados,{spaces: 2},(err) => {
        console.log(err);
        if (err) return reject(err);
        return success(true);
      });
    });
  },
  getDados: () =>{
    return new Promise((success, reject) => {
      if ( !fs.existsSync(arquivo) ) return reject(false);
      jsonFile.readFile(arquivo, (err, obj) => {
        if (err) return reject(false);
        return success(obj);
      });
    });
  },
  salvaDados: (operacao,dados) => {
    return new Promise((success, reject) => {
        self.getDados().then( (obj) => {
            if(operacao =="update"){
                return reject(true);
            }else{
                if(obj && obj[0]){
                    obj.push(dados);
                    self.salvaJson(obj); 
                    return success(true);
                }else{
                    let array = [];
                    array.push(dados);
                    self.salvaJson(array); 
                    return success(true);
                }
                
            }
        }).catch( (err) => {
            let array = [];
            array.push(dados);
            self.salvaJson(array); 
            return success(true);
        });
    });
  }
}
